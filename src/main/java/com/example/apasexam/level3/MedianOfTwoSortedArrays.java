package com.example.apasexam.level3;

import java.util.Arrays;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Sompong Thongmee
 * @create 01/09/2022 13:39
 * @project apas-exam
 */
@Slf4j
public class MedianOfTwoSortedArrays {

  public static void main(String[] args) {
    int[] num1 = new int[]{2, 3, 6, 6, 33, 44, 99};
    int[] num2 = new int[]{4, 5, 7, 9};
    double v = findMedianSortedArrays(num1, num2);
    log.info("v = {}", v);
  }

  public static double medianOfTwoSortedArrays(int[] num1, int[] num2) {
    int[] result = Arrays.copyOf(num1, num1.length + num2.length);
    System.arraycopy(num2, 0, result, num1.length, num2.length);
    Arrays.sort(result);
    log.info("result = {}", Arrays.toString(result));
    int mid = result.length / 2;
    if (result.length % 2 == 0) {
      return (result[mid - 1] + result[mid]) / 2.0;
    }
    return result[mid];
  }

  @SuppressWarnings("java:S3776")
  public static double findMedianSortedArrays(int[] a, int[] b) {
    int m = a.length;
    int n = b.length;
    if (m > n) { // to ensure m<=n
      int[] temp = a;
      a = b;
      b = temp;
      int tmp = m;
      m = n;
      n = tmp;
    }
    int iMin = 0;
    int iMax = m;
    int halfLen = (m + n + 1) / 2;
    int count = 0;
    while (iMin <= iMax) {
      log.info("count = {}", count);
      count++;
      int i = (iMin + iMax) / 2;
      int j = halfLen - i;
      if (i < iMax && b[j - 1] > a[i]) {
        iMin = i + 1; // i is too small
      } else if (i > iMin && a[i - 1] > b[j]) {
        iMax = i - 1; // i is too big
      } else { // i is perfect
        int maxLeft = 0;
        if (i == 0) {
          maxLeft = b[j - 1];
        } else if (j == 0) {
          maxLeft = a[i - 1];
        } else {
          maxLeft = Math.max(a[i - 1], b[j - 1]);
        }
        if ((m + n) % 2 == 1) {
          return maxLeft;
        }

        int minRight = 0;
        if (i == m) {
          minRight = b[j];
        } else if (j == n) {
          minRight = a[i];
        } else {
          minRight = Math.min(b[j], a[i]);
        }

        return (maxLeft + minRight) / 2.0;
      }
    }
    return 0.0;
  }


}
