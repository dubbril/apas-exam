package com.example.apasexam.level2;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Sompong Thongmee
 * @create 30/08/2022 11:31
 * @project apas-exam
 */
@Getter
@Setter
@NoArgsConstructor
public class ListNode<E> {

  private E value;
  private ListNode<E> next;

  public ListNode(E value) {
    this.value = value;
  }

  @Override
  public String toString() {
    String str = "result = [" + value.toString();
    while (next != null) {
      str = str.concat(", ").concat(next.getValue().toString());
      next = next.getNext();
    }
    return str+"]";
  }
}
