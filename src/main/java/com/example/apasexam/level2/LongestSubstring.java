package com.example.apasexam.level2;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Sompong Thongmee
 * @create 30/08/2022 13:29
 * @project apas-exam
 */
@Slf4j
public class LongestSubstring {

  public static void main(String[] args) {
    int result = longestSubstring("pwwkew");
    log.info("result = {}", result);
  }

  public static int longestSubstring(String str) {
    int n = str.length();
    int ans = 0;
    int[] index = new int[128];
    int i = 0;
    for (int j = 0; j < n; j++) {
      i = Math.max(index[str.charAt(j)], i);
      ans = Math.max(ans, j - i + 1);
      index[str.charAt(j)] = j + 1;
    }
    return ans;
  }

}
