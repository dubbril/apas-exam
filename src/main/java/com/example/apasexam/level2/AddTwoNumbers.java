package com.example.apasexam.level2;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;


/**
 * @author Sompong Thongmee
 * @create 29/08/2022 23:06
 * @project apas-exam
 */
@Slf4j
public class AddTwoNumbers {

  public static void main(String[] args) {
    List<Integer> l1 = new ArrayList<>();
    l1.add(2);
    l1.add(4);
    l1.add(3);
    LinkedList<Integer> l2 = new LinkedList<>();
    l2.add(5);
    l2.add(6);
    l2.add(4);
    List<Integer> resultList = addTwoNumber(l1, l2);
    log.info("result = " + resultList);

    ListNode<Integer> i1 = new ListNode<>(2);
    ListNode<Integer> i2 = new ListNode<>(4);
    ListNode<Integer> i3 = new ListNode<>(3);
    i2.setNext(i3);
    i1.setNext(i2);

    ListNode<Integer> j1 = new ListNode<>(5);
    ListNode<Integer> j2 = new ListNode<>(6);
    ListNode<Integer> j3 = new ListNode<>(4);
    j2.setNext(j3);
    j1.setNext(j2);

    ListNode<Integer> resultListNode = addTwoNumber(i1, j1);
    log.info(resultListNode.toString());

  }

  /* input (2 -> 4 -> 3) + (5 -> 6 -> 4)
   * output 7 -> 0 -> 8
   * 342 + 465 = 807
   */
  public static List<Integer> addTwoNumber(List<Integer> l1, List<Integer> l2) {
    List<Integer> result = new LinkedList<>();
    int max = Math.max(l1.size(), l2.size());
    int remain = 0;
    for (int i = 0; i < max; i++) {
      int x = i < l1.size() ? l1.get(i) : 0;
      int y = i < l2.size() ? l2.get(i) : 0;
      int carry = x + y + remain;
      remain = 0;
      if (carry >= 10) {
        remain = 1;
        carry = carry - 10;
      }
      result.add(carry);
    }
    if (remain > 0) {
      result.add(remain);
    }
    return result;
  }

  public static ListNode<Integer> addTwoNumber(ListNode<Integer> l1, ListNode<Integer> l2) {
    var dummyHead = new ListNode<>(0);
    var p = l1;
    var q = l2;
    var curr = dummyHead;
    int carry = 0;
    while (p != null || q != null) {
      int x = (p != null) ? p.getValue() : 0;
      int y = (q != null) ? q.getValue() : 0;
      int sum = carry + x + y;
      carry = sum / 10;
      curr.setNext(new ListNode<>(sum % 10));
      curr = curr.getNext();
      if (p != null) {
        p = p.getNext();
      }
      if (q != null) {
        q = q.getNext();
      }
    }

    if (carry > 0) {
      curr.setNext(new ListNode<>(carry));
    }
    return dummyHead.getNext();
  }


}
