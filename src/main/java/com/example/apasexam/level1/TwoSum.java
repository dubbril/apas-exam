package com.example.apasexam.level1;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;


/**
 * @author Sompong Thongmee
 * @create 29/08/2022 22:16
 * @project apas-exam
 */
@Slf4j
public class TwoSum {

  public static void main(String[] args) {
    int[] nums = new int[]{2, 7, 11, 15, 4, 1, 9, 8};
    int target = 12;

    int[] result = twoSum(nums, target);
    log.info("result = {}", Arrays.toString(result));
  }


  public static int[] twoSum(int[] nums, int target) {
    Map<Integer, Integer> map = new HashMap<>();
    for (int i = 0; i < nums.length; i++) {
      int complement = target - nums[i];
      if (map.containsKey(complement)) {
        return new int[]{map.get(complement), i};
      }
      map.put(nums[i], i);
    }
    throw new IllegalArgumentException("No two sum solution");
  }
}
