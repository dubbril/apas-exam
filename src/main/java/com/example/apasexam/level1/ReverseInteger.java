package com.example.apasexam.level1;

import lombok.extern.slf4j.Slf4j;

@Slf4j
/**
 * @author Sompong Thongmee
 * @create 01/09/2022 15:49
 * @project apas-exam
 */
public class ReverseInteger {

  public static void main(String[] args) {
    int integer = reverseInteger(-123456);
    log.info("integer = {}",integer);
  }

  public static Integer reverseInteger1(int integer) {
    String sign = integer < 0 ? "-" : "";
    int abs = Math.abs(integer);
    String s = sign + new StringBuilder().append(abs).reverse();
    return Integer.parseInt(s);
  }

  public static int reverseInteger(int x) {
    long out = 0;
    while (x != 0) {
      out = out * 10 + x % 10;
      x = x / 10;
    }
    if (out > Integer.MAX_VALUE || out < Integer.MIN_VALUE) {
      return 0;
    }

    return (int) out;

  }

}
